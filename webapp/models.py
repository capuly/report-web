from __future__ import unicode_literals

from django.db import models



class TimeStampModel(models.Model):
	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now_add=True)
	class Meta:
		abstract =True


class Profugo(TimeStampModel):
	nombre = models.CharField(max_length=100)
	foto = models.CharField(max_length=300)
	descipcion = models.CharField(max_length=300)
	def __unicode__(self): 
		return "%s" % (self.nombre)


class Reporte(TimeStampModel):
	informate = models.CharField(max_length=100)
	profugo = models.ForeignKey(Profugo)

	latitude = models.CharField(max_length=25)
	longitude = models.CharField(max_length=25)
	horizontalAccuracy = models.CharField(max_length=25)
	altitude = models.CharField(max_length=25)
	verticalAccuracy = models.CharField(max_length=25)
	distance = models.CharField(max_length=25)
	def __unicode__(self): 
		return "%s - %s : (Lat: %s - Lng: %s)" % (self.informate, self.profugo, self.latitude, self.longitude)











