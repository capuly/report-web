from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers



from .views import *

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
#router.register(r'users', UserViewSet)
#router.register(r'profugos', ProfugoViewSet)
#router.register(r'reportes', ReporteViewSet)



urlpatterns = [
	url(r'^', include(router.urls)),
	url(r'^profugos/$', ProfugoList.as_view()),
	url(r'^reportes/$', ReporteList.as_view()),
	#url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

	#url(r'^$', views.index, name='index')
]
