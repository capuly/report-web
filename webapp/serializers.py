from django.conf.urls import url, include
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets

from .models import *

# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')


class ProfugoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profugo
        fields = ('id', 'nombre', 'foto', 'descipcion')


class ReporteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reporte
        fields = ('profugo', 'informate', 'latitude', 'longitude', 'horizontalAccuracy', 'verticalAccuracy', 'altitude', 'distance')

