#-*- coding: utf-8 -*-
from django.shortcuts import render,render_to_response,get_object_or_404, redirect
from rest_framework import routers, serializers, viewsets
from django.contrib.auth.models import User

#from .forms import UserRegisterFrom
from .models import *
from .serializers import *

def index(request):
    return render(request, 'index.html',)


# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class ProfugoViewSet(viewsets.ModelViewSet):
    queryset = Profugo.objects.all()
    serializer_class = ProfugoSerializer

'''class ReporteViewSet(viewsets.ModelViewSet):
    queryset = Reporte.objects.all()
    serializer_class = ReporteSerializer'''


from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, IsAdminUser


class ProfugoList(APIView):
    """
    List all Profugos, or create a new Profugo.
    """
    permission_classes = (IsAuthenticated,)
    
    def get(self, request, format=None):
        profugos = Profugo.objects.all()
        serializer = ProfugoSerializer(profugos, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ProfugoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ReporteList(APIView):
    """
    List all Reportes, or create a new Reporte.
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        reportes = Reporte.objects.all()
        serializer = ReporteSerializer(reportes, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ReporteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
